import java.util.Scanner;

public class ejercicio_2 {

    public static int calcularMCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);
        }
    }

    public static int calcularMCDN(int[] numeros, int n) {
        if (n == 1) {
            return numeros[0];
        } else {
            return calcularMCD(numeros[n - 1], calcularMCDN(numeros, n - 1));
        }
    }

    public static void main(String[] args) {
        Scanner scm = new Scanner(System.in);
        System.out.print("ingrese la cantidad de numeros: ");
        int n = scm.nextInt(); 
        int[] numeros = new int[n]; 

        System.out.println("ingrese los " + n + " numeros:");
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i + 1) + ": ");
            numeros[i] = scm.nextInt();
        }

        int mcd = calcularMCDN(numeros, n); 
        System.out.println("el MCD de los " + n + " numeros es: " + mcd);

        scm.close();
    }
}
