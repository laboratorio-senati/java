import java.util.*;

public class ejercicio_7 {

    private static List<Integer> datosGenerados = new ArrayList<>();

    public static void main(String[] args) {
        generarDatosAleatorios(1000, 0, 300);
        
        System.out.println("datos generados: " + datosGenerados);
        
        quickSort(datosGenerados, 0, datosGenerados.size() - 1);
        
        System.out.println("datos ordenados: " + datosGenerados);
    }

    private static void generarDatosAleatorios(int cantidad, int rangoMin, int rangoMax) {
        Random random = new Random();
        for (int i = 0; i < cantidad; i++) {
            int dato = random.nextInt(rangoMax - rangoMin + 1) + rangoMin;
            datosGenerados.add(dato);
        }
    }

    private static void quickSort(List<Integer> datos, int izquierda, int derecha) {
        if (izquierda < derecha) {
            int indiceParticion = particion(datos, izquierda, derecha);
            quickSort(datos, izquierda, indiceParticion - 1);
            quickSort(datos, indiceParticion + 1, derecha);
        }
    }

    private static int particion(List<Integer> datos, int izquierda, int derecha) {
        int pivote = datos.get(derecha);
        int i = izquierda - 1;
        for (int j = izquierda; j < derecha; j++) {
            if (datos.get(j) <= pivote) {
                i++;
                intercambiar(datos, i, j);
            }
        }
        intercambiar(datos, i + 1, derecha);
        return i + 1;
    }

    private static void intercambiar(List<Integer> datos, int i, int j) {
        int temp = datos.get(i);
        datos.set(i, datos.get(j));
        datos.set(j, temp);
    }
}
