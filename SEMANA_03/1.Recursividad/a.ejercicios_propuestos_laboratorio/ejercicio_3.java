import java.util.Scanner;

public class ejercicio_3 {

    public static int calcularMCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calcularMCD(b, a % b);
        }
    }
    public static int calcularLCMN(int[] numeros, int n) {
        if (n == 1) {
            return numeros[0];
        } else {
            int lcm = (numeros[n - 1] * numeros[n - 2]) / calcularMCD(numeros[n - 1], numeros[n - 2]);
            numeros[n - 2] = lcm;
            return calcularLCMN(numeros, n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner scm = new Scanner(System.in);
        System.out.print("ingrese la cantidad de numeros: ");
        int n = scm.nextInt(); 
        int[] numeros = new int[n]; 

        System.out.println("ingrese los " + n + " numeros:");
        for (int i = 0; i < n; i++) {
            System.out.print("numero " + (i + 1) + ": ");
            numeros[i] = scm.nextInt();
        }

        int lcm = calcularLCMN(numeros, n); 
        System.out.println("el LCM de los " + n + " numeros es: " + lcm);

    }
}
