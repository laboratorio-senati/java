import java.io.*;
import java.util.*;

class Persona {
    protected String nombreCompleto;
    protected String grupoPersonas;

    public Persona(String nombreCompleto, String grupoPersonas) {
        this.nombreCompleto = nombreCompleto;
        this.grupoPersonas = grupoPersonas;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public String getGrupoPersonas() {
        return grupoPersonas;
    }

    @Override
    public String toString() {
        return "Nombre completo: " + nombreCompleto + ", Grupo de personas: " + grupoPersonas;
    }
}

class Trabajador extends Persona {
    public Trabajador(String nombreCompleto, String grupoPersonas) {
        super(nombreCompleto, grupoPersonas);
    }
}

public class ejercicio_8 {

    public static void main(String[] args) {
        Trabajador[] trabajadores = leerDatosDesdeArchivo();

        Arrays.sort(trabajadores, new Comparator<Trabajador>() {
            @Override
            public int compare(Trabajador t1, Trabajador t2) {
                return t1.getNombreCompleto().compareTo(t2.getNombreCompleto());
            }
        });

        guardarDatosEnArchivo(trabajadores);
    }

    private static Trabajador[] leerDatosDesdeArchivo() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("clientes.txt"));
            List<Trabajador> trabajadoresList = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                String[] datos = line.split(",");
                String nombreCompleto = datos[0].trim();
                String grupoPersonas = datos[1].trim();
                Trabajador trabajador = new Trabajador(nombreCompleto, grupoPersonas);
                trabajadoresList.add(trabajador);
            }
            br.close();
            return trabajadoresList.toArray(new Trabajador[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new Trabajador[0];
    }

    private static void guardarDatosEnArchivo(Trabajador[] trabajadores) {}
        
}
