import java.util.ArrayList;
import java.util.Scanner;

public class ejercicio_4{
    public static int calcularPermutaciones(ArrayList<Integer> lista) {
        if (lista.size() == 0) {
            return 1;
        } else {
            int numPermutaciones = 0;
            for (int i = 0; i < lista.size(); i++) {
                int elemento = lista.get(i);
                ArrayList<Integer> listaRestante = new ArrayList<>(lista);
                listaRestante.remove(i);
                numPermutaciones += calcularPermutaciones(listaRestante);
            }
            return numPermutaciones;
        }
    }

    public static void main(String[] args) {
        Scanner scm = new Scanner(System.in);
        System.out.print("ingrese la cantidad de elementos: ");
        int n = scm.nextInt(); 
        ArrayList<Integer> lista = new ArrayList<>(); 
        System.out.println("ingrese los " + n + " elementos:");
        for (int i = 0; i < n; i++) {
            System.out.print("elemento " + (i + 1) + ": ");
            int elemento = scm.nextInt();
            lista.add(elemento);
        }

        int numPermutaciones = calcularPermutaciones(lista); 
        System.out.println("el numero de permutaciones de los " + n + " elementos es: " + numPermutaciones);

    }
}
