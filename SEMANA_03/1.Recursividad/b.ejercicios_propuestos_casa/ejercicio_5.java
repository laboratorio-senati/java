public class ejercicio_5 {
    public static void main(String[] args) {
        int altura = 7; 
        for (int i = 1; i <= altura; i++) {
            
            for (int j = altura - i; j > 0; j--) {
                System.out.print(" ");
            }

            for (int k = 1; k <= i; k++) {
                System.out.print("*");
            }

            System.out.println();
        }
    }
}
