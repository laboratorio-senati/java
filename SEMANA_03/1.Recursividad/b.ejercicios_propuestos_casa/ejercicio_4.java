import java.util.Scanner;

public class ejercicio_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ingrese un numero ");
        int n = scanner.nextInt();
        scanner.close();

        System.out.println("Los numeros del 1 al " + n + " en orden inverso son:");
        for (int i = n; i >= 1; i--) {
            System.out.print(i + " ");
        }
    }
}
