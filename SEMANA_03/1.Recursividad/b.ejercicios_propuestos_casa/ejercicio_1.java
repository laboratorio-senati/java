import java.util.Scanner;

public class ejercicio_1 {
    public static void main(String[] args) {
        Scanner scm = new Scanner(System.in);

        System.out.print("ingrese la base: ");
        int base = scm.nextInt(); 

        System.out.print("ingrese el exponente: ");
        int exponente = scm.nextInt(); 

        double resultado = Math.pow(base, exponente);

        System.out.println("el resultado de " + base + " elevado a la potencia " + exponente + " es: " + resultado);
    }
}
