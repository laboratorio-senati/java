public class Persona {
  String nombre; //Atributo que identifica el nombre de una persona
  String apellidos;//Atributo que identifica los apellidos de ina persona
  String numeroDocumentoIdentidad;
  String PaisNacimiento;//Atributo que identofica el pais de una persona
  int añoNacimiento;//Atributo que identifica el año de nacimiento de una persona
  char genero;//Atributo que identifica el genero de una persona
  
  
  Persona(String nombre, String apellidos, String numeroDocumentoIdentidad, String PaisNacimiento, int añoNacimiento, char genero) {
      this.nombre = nombre;
      this.apellidos = apellidos;
      this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
      this.añoNacimiento = añoNacimiento;
      this.PaisNacimiento = PaisNacimiento;
      this.genero = genero;
  }
  void Imprimir(){
    System.out.println("---------------Persona----------------");
    System.out.println("Nombre                   :"+nombre);
    System.out.println("Apellido                 :"+apellidos);
    System.out.println("Numero de documento:     :"+ numeroDocumentoIdentidad);
    System.out.println("Año de nacimiento:       :"+añoNacimiento);
    System.out.println("Genero                   :"+genero);
    System.out.println("Pais de nacimiento       :"+PaisNacimiento);
  }
  public static void main(String[] args) {
    Persona persona1 = new Persona("Jheison", "Forge", "7489516", "Perú", 2003, 'H');     
    Persona persona2 = new Persona("Pedro", "Quispe", "1879214", "China", 2003, 'H');
    persona1.Imprimir();
    persona2.Imprimir();

}
}
