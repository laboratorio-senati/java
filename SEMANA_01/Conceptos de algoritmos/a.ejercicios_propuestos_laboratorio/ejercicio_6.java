import java.util.Scanner;

public class ejercicio_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa tu peso en kilogramos: ");
        double peso = scanner.nextDouble();
        System.out.print("Ingresa tu estatura en metros: ");
        double estatura = scanner.nextDouble();

        double imc = peso / (estatura * estatura);

        System.out.println("Tu índice de masa corporal (IMC) es: " + imc);
    }
}

