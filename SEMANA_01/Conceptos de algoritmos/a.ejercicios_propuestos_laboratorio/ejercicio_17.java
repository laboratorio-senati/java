import java.util.Scanner;

public class ejercicio_17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese la lista de palabras separadas por espacios:");
        String[] palabras = sc.nextLine().split(" ");
       
        System.out.println("Ingrese el número determinado:");
        int umbral = sc.nextInt();
          
        System.out.println("Los factores de cada palabra que tienen una longitud mayor que " + umbral + " son:");
        for (String palabra : palabras) {
            for (int i = 1; i <= palabra.length(); i++) {
                String factor = palabra.substring(0, i);
                if (factor.length() > umbral) {
                    System.out.println(factor);
                }
            }
        }
    }
}
