import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ejercicio_14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa una lista de palabras separadas por espacios: ");
        String input = scanner.nextLine();
        String[] words = input.split(" ");

        List<String> palindromes = new ArrayList<>();
        for (String word : words) {
            if (esPalindromo(word)) {
                palindromes.add(word);
            }
        }

        System.out.println("Encontré " + palindromes.size() + " palabras palíndromas en la lista:");
        for (String palindrome : palindromes) {
            System.out.println(palindrome);
        }
    }

    public static boolean esPalindromo(String palabra) {
        palabra = palabra.replaceAll("\\s+", "").toLowerCase();

        int i = 0;
        int j = palabra.length() - 1;
        while (i < j) {
            if (palabra.charAt(i) != palabra.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }

        return true;
    }
}
