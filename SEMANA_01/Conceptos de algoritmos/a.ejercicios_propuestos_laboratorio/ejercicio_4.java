import java.util.Scanner;
public class ejercicio_4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa el primer lado: ");
        int lado1 = scanner.nextInt();
        System.out.print("Ingresa el segundo lado: ");
        int lado2 = scanner.nextInt();
        System.out.print("Ingresa el tercer lado: ");
        int lado3 = scanner.nextInt();

        if (lado1 + lado2 > lado3 && lado1 + lado3 > lado2 && lado2 + lado3 > lado1) {
            System.out.println("Los lados ingresados pueden formar un triángulo válido.");
        } else {
            System.out.println("Los lados ingresados no pueden formar un triángulo válido.");
        }
    }
}
