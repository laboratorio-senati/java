import java.util.Scanner;

public class ejercicio_13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String input = scanner.nextLine();

        String[] numerosString = input.split(" ");
        int contadorNumerosFibonacci = 0;

        for (String numeroString : numerosString) {
            int numero = Integer.parseInt(numeroString);
            if (esNumeroDeFibonacci(numero)) {
                contadorNumerosFibonacci++;
            }
        }

        System.out.println("Hay " + contadorNumerosFibonacci + " números de Fibonacci en la lista.");
    }

    public static boolean esNumeroDeFibonacci(int numero) {
        int a = 0;
        int b = 1;
        while (b < numero) {
            int c = a + b;
            a = b;
            b = c;
        }
        return b == numero;
    }
}
