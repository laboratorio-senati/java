import java.util.Scanner;

public class ejercicio_20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
     
        System.out.println("Ingrese la lista de números enteros separados por comas:");
        String numerosString = sc.nextLine();
        String[] numerosArray = numerosString.split(",");
        int[] numeros = new int[numerosArray.length];
        for (int i = 0; i < numerosArray.length; i++) {
            numeros[i] = Integer.parseInt(numerosArray[i].trim());
        }
        
        System.out.println("Ingrese la cantidad mínima de divisores:");
        int cantDivisoresMin = sc.nextInt();
    
        int cantNumerosConDivisores = 0;
        for (int i = 0; i < numeros.length; i++) {
            int cantDivisores = 0;
            for (int j = 1; j <= numeros[i]; j++) {
                if (numeros[i] % j == 0) {
                    cantDivisores++;
                }
            }
            if (cantDivisores > cantDivisoresMin) {
                cantNumerosConDivisores++;
            }
        }
        
        System.out.println("La cantidad de números con una cantidad de divisores mayor que " + cantDivisoresMin + " es: " + cantNumerosConDivisores);
    }
}

