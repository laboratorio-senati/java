import java.util.Scanner;

public class ejercicio_7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa una cadena de caracteres: ");
        String cadena = scanner.nextLine();

        String cadenaInvertida = "";
        for (int i = cadena.length() - 1; i >= 0; i--) {
            cadenaInvertida += cadena.charAt(i);
        }

        if (cadena.equalsIgnoreCase(cadenaInvertida)) {
            System.out.println("La cadena ingresada es un palíndromo.");
        } else {
            System.out.println("La cadena ingresada no es un palíndromo.");
        }
    }
}

