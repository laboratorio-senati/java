import java.util.Scanner;

public class ejercicio_12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String input = scanner.nextLine();

        String[] numerosString = input.split(" ");
        int contadorNumerosPerfectos = 0;

        for (String numeroString : numerosString) {
            int numero = Integer.parseInt(numeroString);
            if (esNumeroPerfecto(numero)) {
                contadorNumerosPerfectos++;
            }
        }

        System.out.println("Hay " + contadorNumerosPerfectos + " números perfectos en la lista.");
    }

    public static boolean esNumeroPerfecto(int numero) {
        int sumaDivisores = 0;
        for (int i = 1; i < numero; i++) {
            if (numero % i == 0) {
                sumaDivisores += i;
            }
        }
        return sumaDivisores == numero;
    }
}
