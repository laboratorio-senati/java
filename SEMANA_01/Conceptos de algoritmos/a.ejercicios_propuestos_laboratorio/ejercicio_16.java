import java.util.Scanner;

public class ejercicio_16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese la lista de palabras separadas por espacios:");
        String[] palabras = sc.nextLine().split(" ");
        
        System.out.println("Ingrese la letra determinada:");
        char letra = sc.nextLine().charAt(0);

        System.out.println("Las palabras que contienen la letra " + letra + " son:");
        for (String palabra : palabras) {
            if (palabra.indexOf(letra) != -1) {
                System.out.println(palabra);
            }
        }
    }
}
