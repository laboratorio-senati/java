import java.util.Scanner;

public class ejercicio_11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String input = scanner.nextLine();

        String[] numerosString = input.split(" ");
        int contadorMultiplosDeTres = 0;

        for (String numeroString : numerosString) {
            int numero = Integer.parseInt(numeroString);
            if (numero % 3 == 0) {
                contadorMultiplosDeTres++;
            }
        }

        System.out.println("Hay " + contadorMultiplosDeTres + " números múltiplos de 3 en la lista.");
    }
}

