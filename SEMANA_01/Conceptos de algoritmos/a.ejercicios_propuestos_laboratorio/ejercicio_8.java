import java.util.Scanner;

public class ejercicio_8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa una fecha en formato dd/mm/yyyy: ");
        String fecha = scanner.nextLine();

        boolean esFechaValida = true;
        try {
            String[] partesFecha = fecha.split("/");
            int dia = Integer.parseInt(partesFecha[0]);
            int mes = Integer.parseInt(partesFecha[1]);
            int anio = Integer.parseInt(partesFecha[2]);

            if (dia < 1 || dia > 31 || mes < 1 || mes > 12 || anio < 0) {
                esFechaValida = false;
            }
        } catch (Exception e) {
            esFechaValida = false;
        }

        if (esFechaValida) {
            System.out.println("La fecha ingresada es válida.");
        } else {
            System.out.println("La fecha ingresada no es válida.");
        }
    }
}

