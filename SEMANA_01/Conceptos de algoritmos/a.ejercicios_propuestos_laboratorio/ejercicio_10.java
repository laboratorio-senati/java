import java.util.Scanner;

public class ejercicio_10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa una lista de números enteros separados por espacios: ");
        String input = scanner.nextLine();

        String[] numerosString = input.split(" ");
        int contadorPares = 0;

        for (String numeroString : numerosString) {
            int numero = Integer.parseInt(numeroString);
            if (numero % 2 == 0) {
                contadorPares++;
            }
        }

        System.out.println("Hay " + contadorPares + " números pares en la lista.");
    }
}

