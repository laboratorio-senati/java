import java.util.Scanner;

public class ejercicio_15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese la lista de palabras separadas por espacios:");
        String[] palabras = sc.nextLine().split(" ");
        
        System.out.println("Ingrese la letra determinada:");
        char letra = sc.nextLine().charAt(0);
        
        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.charAt(0) == letra) {
                contador++;
            }
        }

        System.out.println("Hay " + contador + " palabras que comienzan con la letra " + letra);
    }
}
