import java.util.Scanner;

public class ejercicio_19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese un número entero:");
        int n = sc.nextInt();
        
        double suma = 0;
        for (int i = 1; i <= n; i++) {
            suma += 1.0 / i;
        }
        
        if (suma == n) {
            System.out.println(n + " es un número armónico.");
        } else {
            System.out.println(n + " no es un número armónico.");
        }
    }
}

