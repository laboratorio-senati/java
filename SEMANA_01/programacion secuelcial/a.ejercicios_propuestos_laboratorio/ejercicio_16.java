import java.util.Scanner;

public class ejercicio_16 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Ingrese el primer número");
        double num1 = input.nextDouble();

        System.out.println("Ingrese el segundo número");
        double num2 = input.nextDouble();

        System.out.println("Ingrese la operación");
        char operacion = input.next().charAt(0);

        double resultado = 0;

        switch (operacion) {
            case '+':
                resultado = num1 + num2;
                break;
            case '-':
                resultado = num1 - num2;
                break;
            case '*':
                resultado = num1 * num2;
                break;
            case '/':
                resultado = num1 / num2;
                break;
        }
        System.out.println("El resultado de la operación es: " + resultado);
    }
}
