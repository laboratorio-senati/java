import java.util.Scanner;

public class ejercicio_17{
   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);
      System.out.print("Ingrese el presupuesto del hospital: ");
      double presupuesto = input.nextDouble();

      double preOncologia = presupuesto * 0.55;
      double prePedriatia = presupuesto * 0.20;
      double preTraumatologia = presupuesto * 0.25;

      System.out.println(" el presupuesto de Oncologia es :" + preOncologia);
      System.out.println(" el presupuesto de Pedriatria es :" + prePedriatia);
      System.out.println(" el presupuesto de Traumatologia es :" + preTraumatologia);
    
   }
}