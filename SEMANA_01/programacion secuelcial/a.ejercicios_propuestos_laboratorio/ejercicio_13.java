
import java.util.Scanner;

public class ejercicio_13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese la temperatura en grados Celsius");
        double celsius = sc.nextDouble();
        
        double fahrenheit = 9/5 * celsius + 32;
        
        System.out.println(celsius + " grados Celsius equivale a " + fahrenheit + " grados Fahrenheit.");
    }
}