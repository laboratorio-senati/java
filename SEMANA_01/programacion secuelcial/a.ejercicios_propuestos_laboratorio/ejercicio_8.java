import java.util.Scanner;

public class ejercicio_8 {
   public static void main(String[] args) {
      
      Scanner input = new Scanner(System.in);

      System.out.println("Ingrese la longitud de la base mayor:");
      int baseMayor = input.nextInt();
      
      System.out.println("Ingrese la longitud de la base menor:");
      int baseMenor = input.nextInt();
      
      System.out.println("Ingrese la altura:");
      int altura = input.nextInt();
      
      System.out.println("Ingrese la longitud del primer lado no paralelo:");
      int lado1 = input.nextInt();
      
      System.out.println("Ingrese la longitud del segundo lado no paralelo:");
      int lado2 = input.nextInt();
  
      double area = ((baseMayor + baseMenor) * altura) / 2;

      System.out.println("El área del trapecio es: " + area);
      
      input.close();
   }
}
