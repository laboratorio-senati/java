import java.util.Scanner;

public class ejercicio_18{
   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);
      double totalCompra, descuento, precioFinal;
      
      System.out.print("Ingrese el monto de la compra: ");
      totalCompra = input.nextDouble();
      
      descuento = 0.15 * totalCompra;
      
      precioFinal = totalCompra - descuento;
      
      System.out.println("El precio final es: " + precioFinal);
   }
}
