/* Programa Java que lea dos números enteros por teclado y los muestre
 por pantalla. */

import java.util.Scanner;

public class ejercicio_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("ingrese el primer numero: ");
        int numero1 = sc.nextInt();

        System.out.print("ingrese el segundo numero: ");
        int numero2 = sc.nextInt();

        System.out.println("el primer numero ingresado es: " + numero1);
        System.out.println("el segundo numero ingresado es: " + numero2);

        sc.close();
    }
}

