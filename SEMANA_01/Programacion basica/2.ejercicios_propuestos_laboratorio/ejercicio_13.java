//Crea un programa que permita al usuario ingresar una cadena de texto y la imprima en la consola.
import java.util.Scanner;

public class ejercicio_13 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese una cadena de texto: ");
        String texto = entrada.nextLine();
        
        System.out.println("La cadena de texto ingresada es: " + texto);
        
        entrada.close();
    }
}


