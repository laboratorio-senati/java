// Crea un programa que permita al usuario ingresar su edad 
// y determine si es mayor de edad o no.
import java.util.Scanner;

public class ejercicio_14 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese su edad: ");
        int edad = entrada.nextInt();
        
        if (edad >= 18) {
            System.out.println("Usted es mayor de edad");
        } else {
            System.out.println("Usted es menor de edad");
        }
        
        entrada.close();
    }
}