//Crea un programa que permita al usuario ingresar dos números enteros y determine cuál de los dos es el mayor.
import java.util.Scanner;

public class ejercicio_11 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = entrada.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = entrada.nextInt();
        
        if (numero1 > numero2) {
            System.out.println(numero1 + " es mayor que " + numero2);
        } else if (numero2 > numero1) {
            System.out.println(numero2 + " es mayor que " + numero1);
        } else {
            System.out.println(numero1 + " y " + numero2 + " son iguales");
        }
        
        entrada.close();
    }
}
