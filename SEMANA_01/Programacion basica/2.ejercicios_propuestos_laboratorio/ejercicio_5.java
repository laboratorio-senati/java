import java.util.Scanner;

public class ejercicio_5 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Ingrese el primer número: ");
        int num1 = sc.nextInt();
        
        System.out.println("Ingrese el segundo número: ");
        int num2 = sc.nextInt();
        
        int diferencia = num1 - num2;
        
        System.out.println("La diferencia entre " + num1 + " y " + num2 + " es " + diferencia);
        
        sc.close();
    }
}

