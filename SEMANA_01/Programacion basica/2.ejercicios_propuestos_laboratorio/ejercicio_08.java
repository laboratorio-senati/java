//Crea un programa que permita al usuario ingresar la longitud de los lados de un triángulo y determine si es equilátero, isósceles o escaleno.
import java.util.Scanner;

public class ejercicio_08 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        System.out.print("Ingrese la longitud del lado 1: ");
        double lado1 = entrada.nextDouble();
        
        System.out.print("Ingrese la longitud del lado 2: ");
        double lado2 = entrada.nextDouble();
        
        System.out.print("Ingrese la longitud del lado 3: ");
        double lado3 = entrada.nextDouble();
        
        if (lado1 == lado2 && lado2 == lado3) {
            System.out.println("El triángulo es equilátero");
        } else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
            System.out.println("El triángulo es isósceles");
        } else {
            System.out.println("El triángulo es escaleno");
        }
        
        entrada.close();
    }
}
