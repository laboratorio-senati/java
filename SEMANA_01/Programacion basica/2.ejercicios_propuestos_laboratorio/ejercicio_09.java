//Crea un programa que permita al usuario ingresar el radio de un círculo y calcule su área y su circunferencia.
import java.util.Scanner;

public class ejercicio_09 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese el radio del círculo: ");
        double radio = entrada.nextDouble();
        
        double area = Math.PI * Math.pow(radio, 2);
        System.out.println("El área del círculo es: " + area);
        
        double circunferencia = 2 * Math.PI * radio;
        System.out.println("La circunferencia del círculo es: " + circunferencia);
        
        entrada.close();
    }
}

