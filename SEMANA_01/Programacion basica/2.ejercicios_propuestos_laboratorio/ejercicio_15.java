// Crea un programa que permita al usuario ingresar un 
// número entero y determine si es primo o no.
import java.util.Scanner;

public class ejercicio_15 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int numero = entrada.nextInt();
        
        boolean esPrimo = true;
        
        if (numero > 1) {
            for (int i = 2; i <= numero / 2; i++) {
                if (numero % i == 0) {
                    esPrimo = false;
                    break;
                }
            }
        } else {
            esPrimo = false;
        }
        
        if (esPrimo) {
            System.out.println("El número ingresado es primo");
        } else {
            System.out.println("El número ingresado no es primo");
        }
        
        entrada.close();
    }
}
