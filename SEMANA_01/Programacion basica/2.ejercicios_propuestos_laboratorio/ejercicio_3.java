import java.util.Scanner;

public class ejercicio_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = input.nextInt();
        
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = input.nextInt();
        
        int suma = numero1 + numero2;
        System.out.println("La suma de " + numero1 + " y " + numero2 + " es: " + suma);
        
        input.close();
    }
}


