//Crea un programa que permita al usuario ingresar dos números enteros y determine si son iguales o diferentes.
import java.util.Scanner;

public class ejercicio_12 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int numero1 = entrada.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int numero2 = entrada.nextInt();
        
        if (numero1 == numero2) {
            System.out.println(numero1 + " y " + numero2 + " son iguales");
        } else {
            System.out.println(numero1 + " y " + numero2 + " son diferentes");
        }
        
        entrada.close();
    }
}
