//Crea una variable de tipo "Calendar" que contenga la fecha actual.
import java.util.Calendar;

public class ejercicio_4 {
    public static void main(String[] args) {
        Calendar fechaActual = Calendar.getInstance();

        System.out.println("Fecha actual: " + fechaActual.getTime());
    }
}

