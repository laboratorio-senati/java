//Crea una variable de tipo "File" que contenga la ruta de un archivo.
import java.io.File;

public class ejercicio_5 {
    public static void main(String[] args) {
        String rutaArchivo = "C:/carpeta/archivo.txt";
        File archivo = new File(rutaArchivo);

        System.out.println("Ruta del archivo: " + archivo.getAbsolutePath());
    }
}
