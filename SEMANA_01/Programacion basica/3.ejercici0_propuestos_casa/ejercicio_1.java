//Crea una variable de tipo "ArrayList" que contenga una lista de nombres de personas.
import java.util.ArrayList;

public class ejercicio_1 {
    public static void main(String[] args) {
        ArrayList<String> nombres = new ArrayList<String>();

        nombres.add("Juan");
        nombres.add("María");
        nombres.add("Pedro");

        System.out.println("Lista de nombres: " + nombres);
    }
    
}
