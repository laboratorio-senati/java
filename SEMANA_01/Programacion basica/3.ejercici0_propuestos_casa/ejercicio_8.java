//Crea una variable de tipo "Random" que permita generar números aleatorios.
import java.util.Random;

public class ejercicio_8 {
    public static void main(String[] args) {
        Random rand = new Random();
        
        int num = rand.nextInt(10);
        System.out.println("Número aleatorio: " + num);
        
    }
}
