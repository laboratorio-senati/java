import java.text.DateFormat;
import java.util.Date;

public class ejercicio_12 {
    public static void main(String[] args) {
        Date fecha = new Date();
        DateFormat formatoFecha = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
        String fechaFormateada = formatoFecha.format(fecha);
        System.out.println("Fecha y hora formateadas: " + fechaFormateada);

    }
}
