//Crea una variable de tipo "HashMap" que contenga una lista de nombres de personas y su edad.
import java.util.HashMap;

public class ejercicio_2 {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<String, Integer>();

        personas.put("Juan", 25);
        personas.put("María", 30);
        personas.put("Pedro", 20);
        

        for (String nombre : personas.keySet()) {
            System.out.println(nombre + " tiene " + personas.get(nombre) + " años.");
        }
    }
}