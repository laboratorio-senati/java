import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;

public class ejercicio_13 {
  public static void main(String[] args) {
    String host = "localhost";
    int port = 8080;

    try {
      Socket socket = new Socket(host, port);
      System.out.println("Conexión establecida con éxito.");
    } catch (UnknownHostException e) {
      System.err.println("No se pudo encontrar el host: " + host);
    } catch (IOException e) {
      System.err.println("Error de entrada/salida al conectarse al host: " + host);
    }
  }
}