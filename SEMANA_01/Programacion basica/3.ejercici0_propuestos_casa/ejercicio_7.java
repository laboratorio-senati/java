//Crea una variable de tipo "BufferedReader" que permita leer un archivo de texto
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ejercicio_7 {
    public static void main(String[] args) {
        String archivo = "ruta/al/archivo.txt";
        try {
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);
            
            br.close(); 
        } catch (IOException e) {
            System.out.println("Error al leer el archivo: " + e.getMessage());
        }
    }
}

