//Crea una variable de tipo "Date" que contenga la fecha y hora actuales.
import java.util.Date;

public class ejercicio_3 {
    public static void main(String[] args) {
        Date fechaActual = new Date();

        System.out.println("Fecha y hora actual: " + fechaActual);
    }
}
