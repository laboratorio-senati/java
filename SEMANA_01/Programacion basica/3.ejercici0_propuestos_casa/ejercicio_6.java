//Crea una variable de tipo "Scanner" que permita leer la entrada del usuario por consola.
import java.util.Scanner;

public class ejercicio_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese un valor: ");
        String valor = scanner.nextLine();

        System.out.println("Valor ingresado: " + valor);

        scanner.close();
    }
}

