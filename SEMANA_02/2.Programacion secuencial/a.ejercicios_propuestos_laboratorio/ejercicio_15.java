import java.util.Scanner;

public class ejercicio_15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ingresa un numer: ");
        int numero = scanner.nextInt();
        scanner.close();

        if (numero >= 1 && numero <= 3999) {
            String numeroRomano = convertirANumeroRomano(numero);
            System.out.println("el numero " + numero + " en numeros romanos es: " + numeroRomano);
        } else {
            System.out.println("el numero ingresado esta fuera del rango valido.");
        }
    }

    public static String convertirANumeroRomano(int numero) {
        String[] simbolos = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] valores = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

        StringBuilder numeroRomano = new StringBuilder(); 

        for (int i = 0; i < valores.length; i++) {
            while (numero >= valores[i]) {
                numeroRomano.append(simbolos[i]); 
                numero -= valores[i]; 
            }
        }

        return numeroRomano.toString();
    }
}

