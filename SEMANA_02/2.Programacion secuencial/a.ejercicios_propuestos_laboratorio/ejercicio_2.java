public class ejercicio_2 {
    public static void main(String[] args) {
        int[] arreglo = {2, 3, 4, 5}; 
        int producto = calcularProducto(arreglo);
        System.out.println("El producto de los elementos del arreglo es: " + producto);
    }

    public static int calcularProducto(int[] arreglo) {
        int producto = 1; 
        for (int elemento : arreglo) {
            producto *= elemento; 
        }
        return producto;
    }
}

