import java.util.Scanner;

public class ejercicio_12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("¿Está lloviendo?: ");
        String lluvia = scanner.nextLine().toLowerCase();

        System.out.print("¿Está haciendo mucho viento?: ");
        String viento = scanner.nextLine().toLowerCase();

        String[] respuestas = {lluvia, viento};

        if (respuestas[0].equals("si") && respuestas[1].equals("si")) {
            System.out.println("¡Hace mucho viento para salir con una sombrilla!");
        } else if (respuestas[0].equals("si")) {
            System.out.println("Lleva una sombrilla contigo.");
        } else {
            System.out.println("¡Que tengas un bonito dia!");
        }
    }
}
