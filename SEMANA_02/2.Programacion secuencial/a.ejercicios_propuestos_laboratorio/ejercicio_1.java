public class ejercicio_1 {
    public static int sumaElementos(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        return suma;
    }

    public static void main(String[] args) {
        int[] arreglo = { 1, 2, 3, 4, 5 }; 
        int resultado = sumaElementos(arreglo); 
        System.out.println("La suma de los elementos del arreglo es: " + resultado);
    }
}

