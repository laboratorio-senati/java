import java.util.Scanner;

public class ejercicio_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de estudiantes: ");
        int cantidadEstudiantes = scanner.nextInt();
        
        String[][] estudiantes = new String[cantidadEstudiantes][2]; 

        for (int i = 0; i < cantidadEstudiantes; i++) {
            System.out.print("Ingrese el nombre del estudiante " + (i + 1) + ": ");
            estudiantes[i][0] = scanner.next();
            System.out.print("Ingrese la edad del estudiante " + (i + 1) + ": ");
            estudiantes[i][1] = scanner.next();
        }

        double sumaEdades = 0;
        for (int i = 0; i < cantidadEstudiantes; i++) {
            sumaEdades += Double.parseDouble(estudiantes[i][1]); 
        }
        double promedioEdades = sumaEdades / cantidadEstudiantes;

        System.out.println("La edad promedio de los estudiantes es: " + promedioEdades);
    }
}
