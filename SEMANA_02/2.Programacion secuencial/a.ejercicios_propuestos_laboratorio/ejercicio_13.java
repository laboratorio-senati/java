import java.util.Scanner;

public class ejercicio_13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numero;

        do {
            System.out.print("Por favor, ingrese un número entre 1 y 7: ");
            numero = scanner.nextInt();
        } while (numero < 1 || numero > 7);

        switch (numero) {
            case 1:
                System.out.println("Hoy aprenderemos sobre programación");
                break;
            case 2:
                System.out.println("¿Qué tal tomar un curso de marketing digital?");
                break;
            case 3:
                System.out.println("Hoy es un gran día para comenzar a aprender de diseño");
                break;
            case 4:
                System.out.println("¿Y si aprendemos algo de negocios online?");
                break;
            case 5:
                System.out.println("Veamos un par de clases sobre producción audiovisual");
                break;
            case 6:
                System.out.println("Tal vez sea bueno desarrollar una habilidad blanda");
                break;
            case 7:
                System.out.println("Yo decido distraerme programando");
                break;
            default:
                System.out.println("El número ingresado no está en el rango válido.");
                break;
        }
    }
}
