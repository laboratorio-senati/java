import java.util.HashSet;

public class ejercicio_8 {
    public static void main(String[] args) {
        int[] arreglo = {1, 2, 3, 3, 4, 4, 5, 6, 6, 7, 8, 8, 9, 9}; 
        int[] arregloSinDuplicados = eliminarDuplicados(arreglo);
        
        System.out.println("arreglo original:");
        imprimirArreglo(arreglo);
        
        System.out.println("arreglo sin duplicados:");
        imprimirArreglo(arregloSinDuplicados);
    }

    public static int[] eliminarDuplicados(int[] arreglo) {
        HashSet<Integer> set = new HashSet<>(); 
        for (int elemento : arreglo) {
            set.add(elemento); 
        }
        
        int[] arregloSinDuplicados = new int[set.size()]; 
        int i = 0;
        for (int elemento : set) {
            arregloSinDuplicados[i++] = elemento; 
        }
        
        return arregloSinDuplicados;
    }

    public static void imprimirArreglo(int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + " ");
        }
        System.out.println();
    }
}
