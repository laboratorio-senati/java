import java.util.Scanner;

public class ejercicio_14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingresa el número: ");
        int numero = scanner.nextInt();
        scanner.close();
        
        int[] digitos = obtenerDigitos(numero);
        
        for (int i = digitos.length - 1; i >= 0; i--) {
            System.out.println("[" + i + "] = " + digitos[i]);
        }
    }
    
    public static int[] obtenerDigitos(int numero) {
        int[] digitos = new int[4];
        for (int i = 0; i < 4; i++) {
            digitos[i] = numero % 10;
            numero /= 10;
        }
        return digitos;
    }
}
