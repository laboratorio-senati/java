import java.util.Scanner;

public class ejercicio_16 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un valor para N: ");
        int N = scanner.nextInt();
        scanner.close();
        
        for (int i = 1; i <= N; i++) {
            System.out.print(i + " ");
        }
        System.out.println(); 
    }
}
