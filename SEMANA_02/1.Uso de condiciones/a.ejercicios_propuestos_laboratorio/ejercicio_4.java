/* Escribir un programa que calcule la suma de los primeros n números 
perfectos, donde "N" es un número que el usuario ingresa por teclado. */

import java.util.Scanner;

public class ejercicio_4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ingrese un numero");
        int n = sc.nextInt();
        int suma = 0;
        int contador = 1;
        int numPerfecto = 0;
        while (numPerfecto < n){
            if (esPerfecto(contador)){
                suma += contador;
                numPerfecto++;
            }
            contador ++;
        }
        System.out.println("la suma de los primeros " + n + " numeros perfectos es: " + suma);
        sc.close();
    }
    public static boolean esPerfecto(int num) {
        int sumaDivisores = 0;
        for (int i = 1; i < num; i++){
            if (num % i == 0){
                sumaDivisores += i;
            }
        }
        return sumaDivisores == num;
    }
}
