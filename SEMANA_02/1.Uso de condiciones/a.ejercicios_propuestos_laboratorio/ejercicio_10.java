import java.util.Random;
import java.util.Scanner;

public class ejercicio_10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        
        int numeroAdivinar = random.nextInt(100) + 1; 
        int intentos = 10;
        int numeroIntento;
        boolean acertado = false;
        
        System.out.println("¡Bienvenido al juego de adivinar número!");
        System.out.println("Tienes 10 intentos para adivinar el número del 1 al 100.");
        
        while (intentos > 0 && !acertado) {
            System.out.print("Ingresa tu número: ");
            numeroIntento = scanner.nextInt();
            
            if (numeroIntento == numeroAdivinar) {
                System.out.println("¡Felicidades! ¡Has acertado el número en " + (11 - intentos) + " intentos!");
                acertado = true;
            } else if (numeroIntento < numeroAdivinar) {
                System.out.println("El número a adivinar es mayor. Intentos restantes: " + (--intentos));
            } else {
                System.out.println("El número a adivinar es menor. Intentos restantes: " + (--intentos));
            }
        }
        
        if (!acertado) {
            System.out.println("Has agotado tus 10 intentos. El número a adivinar era: " + numeroAdivinar);
        }
        
        scanner.close();
    }
}
