import java.util.Scanner;

public class ejercicio_12 {
    public static void main(String[] args) {
        Scanner sc =  new Scanner(System.in);
        System.out.println("introdusca la cantidad");
        int m = sc.nextInt(); 

        int[] numeros =new int[m];
        for (int i =0; i < m; i++) {
            System.out.println("introduce el numero " + (i+1) + ":");
            numeros[i] = sc.nextInt();
        }
        int mcm = calcularMCM(numeros);
        System.out.println("el mcm de los "+ m + " numeros es: " + mcm);
    }
    public static int calcularMCM(int[] numeros) {
        int maximo = numeros[0];
        for (int i =1; i <numeros.length; i++) {
            if (numeros[i] > maximo){
                maximo = numeros[i];
            }
        }
        int mcm = maximo;
        boolean encontrado = false;
        while (!encontrado){
            int i;
            for (i = 0; i < numeros.length; i++){
                if (mcm % numeros[i] !=0){
                    break;
                }
            }
            if (i == numeros.length){
                encontrado = true;
            }else{
                mcm += maximo;
            }
    }
    return mcm;
        }
        
}
