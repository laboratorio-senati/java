/* Desarollar un programa que imprima la siguiente salida:



*
**
***
****
*****
******
*******
******** */

import java.util.Scanner;

public class ejercicio_6 {
    public static void main(String[] args) {
        Scanner r = new Scanner(System.in);
        System.out.print("ingrese el numero: ");
        int n = r.nextInt();
        for (int i = 1; i<= n; i++) {
            for (int j = 1; j<= i; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}