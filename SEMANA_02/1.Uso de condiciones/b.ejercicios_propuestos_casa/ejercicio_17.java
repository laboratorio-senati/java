import java.util.Scanner;

public class ejercicio_17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ingrese la cantidad de elementos del vector: ");
        int n = scanner.nextInt();

        int[] vector = new int[n];

        System.out.println("ingrese los elementos del vector uno por uno:");
        for (int i = 0; i < n; i++) {
            System.out.print("elemento " + (i + 1) + ": ");
            vector[i] = scanner.nextInt();
        }
        scanner.close();

        int maximo = vector[0];
        int minimo = vector[0];

        for (int i = 1; i < n; i++) {
            if (vector[i] > maximo) {
                maximo = vector[i];
            }

            if (vector[i] < minimo) {
                minimo = vector[i];
            }
        }

        System.out.println("el valor maximo del vector es: " + maximo);
        System.out.println("el valor minimo del vector es: " + minimo);
    }
}

