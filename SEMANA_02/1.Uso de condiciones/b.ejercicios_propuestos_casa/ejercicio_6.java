import java.util.Scanner;

public class ejercicio_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ingrese la serie de numeros separados por espacios: ");
        String numerosStr = scanner.nextLine();
        scanner.close();

        String[] numerosArr = numerosStr.split(" ");
        int[] numeros = new int[numerosArr.length];
        for (int i = 0; i < numerosArr.length; i++) {
            numeros[i] = Integer.parseInt(numerosArr[i]);
        }

        ordenarInsercion(numeros);

        System.out.println("la serie de numeros ordenada es:");
        for (int num : numeros) {
            System.out.print(num + " ");
        }
    }

    public static void ordenarInsercion(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int valorActual = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > valorActual) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = valorActual;
        }
    }
}
