import java.util.Scanner;

public class ejercicio_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ingrese una palabra: ");
        String palabra = scanner.nextLine();
        scanner.close();

        int i = 0;
        while (i < palabra.length()) {
            char letra = palabra.charAt(i);
            System.out.println("letra " + (i + 1) + ": " + letra);
            i++;
        }
    }
}

