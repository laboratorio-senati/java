public class ejercicio_4 {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            int factorial = calcularFactorial(i);
            System.out.println("el factorial de " + i + " es: " + factorial);
        }
    }

    public static int calcularFactorial(int num) {
        int resultado = 1;
        for (int i = 1; i <= num; i++) {
            resultado *= i;
        }
        return resultado;
    }
}
