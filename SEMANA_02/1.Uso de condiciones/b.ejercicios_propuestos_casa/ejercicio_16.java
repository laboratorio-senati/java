import java.util.Scanner;

public class ejercicio_16 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ingrese un numero entero positivo: ");
        int num = scanner.nextInt();
        scanner.close();

        if (num <= 0) {
            System.out.println("el numero debe ser positivo.");
            return;
        }

        for (int i = 1; i <= num; i++) {
            for (int j = i; j >= 1; j--) {
                System.out.print((2 * j - 1) + " ");
            }
            System.out.println();
        }
    }
}

