import java.util.Scanner;

public class ejercicio_11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ingrese el monto inicial de la compra: ");
        double montoInicial = scanner.nextDouble();
        System.out.print("ingrese el numero de meses para el pago: ");
        int numMeses = scanner.nextInt();

        double montoMensual = calcularMontoMensual(montoInicial, numMeses);
        double montoTotal = calcularMontoTotal(montoMensual, numMeses);

        System.out.println("monto mensual a pagar: S/" + montoMensual);
        System.out.println("monto total a pagar después de " + numMeses + " meses: S/" + montoTotal);
    }

    public static double calcularMontoMensual(double montoInicial, int numMeses) {
        return montoInicial / (Math.pow(2, numMeses) - 1);
    }

    public static double calcularMontoTotal(double montoMensual, int numMeses) {
        return montoMensual * (Math.pow(2, numMeses) - 1);
    }
}

